# Database for restaurant management system

##  Opis projektu
Projekt przedstawia model relacyjnej bazy danych przygotowanej dla systemu przeznaczonego do obsługi restauracji. W ramach projektu został przygotowany również zestaw przykładowych danych.  
Na rys. 1 został zamieszczony diagram encji.

![entity_relationship_diagram.JPG](entity_relationship_diagram.jpg)

rys. 1. Diagram encji